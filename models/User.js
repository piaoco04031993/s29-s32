// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required!']
	},
	email: {
		type: String,
		required: [true, 'Email is Required!']
	}, 
	password: {
		type: String,
		required: [true, 'Password is Required!']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No. is Required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course Id is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Enrolled'
			}
		}
	]
});

// model for users
module.exports = mongoose.model('User',userSchema);
	


