// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const auth = require('../auth')

// [SECTION] Functionalities []
 	module.exports.registerUser = (data) => {
 		let fName = data.firstName;
 		let lName = data.lastName;
 		let email = data.email;
 		let passW = data.password;
 		let mobil = data.mobileNo;

 		let newUser = new User({
 			firstName: fName,
 			lastName: lName,
 			email: email,
 			password: bcrypt.hashSync(passW, 10),
 			mobileNo: mobil
 		});
 		return newUser.save().then((user, err) =>{
 			if (user) {
 			  return user;
 			} else {
 			  return false;
 			}
 		});
 	}

 	// Email Checker
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email Already Exists';
			} else {
				return 'Email is still available';
			};
		});
	};

		// login ()
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			let passW = result?.password;
			if (result === null) {
				return false;
			} else {

				const isMatched = bcrypt.compareSync(uPassW, passW)
				if (isMatched) {
					let dataNiUser = result.toObject();
					// console.log(data);
					return {accessToken: auth.createAccessToken(dataNiUser)};
				} else {
					return false;
				}
			   
			};
		});
	};
	  module.exports.enroll = async (data) => {
      let id = data.userId;
      let course = data.courseId;
      let isUserUpdated = await User.findById(id).then(user => {
         const foundSubject = user.enrollments.some(item => item.courseId === course)
         if (!foundSubject) {
            user.enrollments.push({courseId: course});
            return user.save().then((save, error) => {
             if (error) {
                return false;
             } else {
                return true; 
             }
         });
      } else {
         return 'User is Already Enrolled'
      }
         
   });
      return isUserUpdated;
      
     // let isCourseUpdated = await Course.findById(course).then(course => {
     //       course.enrollees.push({userId: id});

     //       return course.save().then((saved, err) => {
     //          if (err) {
     //             return false;
     //          } else {
     //             return true; 
     //          }; 
     //       });
     // }); 
     // if (isUserUpdated && isCourseUpdated) {
     //    return true; //enrollment successful
     // } else {
     //    return 'Enrollment Failed, Go to Registrar';
     // };

  };

// [SECTION] Functionalities [Retrieve]
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};
		module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return admin.save().then((updatedUser, saveErr) => {
 					if (saveErr) {
 						return false;
 					} else {
 						return updatedUser;
 					}
 					

 				})
			} else {
				return 'Updates Failed to implement';
			}
		});
	};
	
	// Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) => {
		let update = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, update).then((user,err) => {
			if (user) {
				return true;
			} else {
				return 'Failed to update user';
			};
		});
	};





			


