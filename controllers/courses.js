//[SECTION] Dependencies and Modules
  	const Course = require('../models/Course');

//[SECTION] Functionality [Create] 
	module.exports.addCourse = (info) => {
		//the info -> will come in form of an object structure
		console.log(info); //check the structure of the data object.
		let isAdmin = info.isAdmin;
		   //isAdmin = true => administrator
		   //isAdmin = false =? regular user
		let course = info.course;
		if (isAdmin) {
			//if user is an Admin, permit to create a new course
			//intelligent paste (ctrl + shift + v)

			let cName = course.name;
			let cDesc = course.description;
			let cCost = course.price;
			let newCourse = new Course({
				name: cName,
				description:cDesc,
				price: cCost
			}); 
			return newCourse.save().then((savedCourse, err) => {
				if (savedCourse) {
					return {course: savedCourse,isAdmin}; 
				} else {
					return false;
				}
			});
		} else {
			//Non-admin
			return 'User Not Permitted'; 
		};
	};

//[SECTION] Functionality [Retrieve] 
   module.exports.getAllCourse = () => {
   		return Course.find({}).then(result => {
   			return result; 
   		});
   };

   module.exports.getAllActive = () => {
   		return Course.find({isActive: true}).then(result => {
   			return result;
   		});
   };

   module.exports.getCourse = (id) => { 
   		return Course.findById(id).then(result => {
   			return result; 
   		});
   };

//[SECTION] Functionality [Update]
    module.exports.updateCourse = (course, details) => {
		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;
		let updatedCourse = {		
			name: cName,
			description: cDesc,
			price: cCost
		}; 
		let id = course.courseId;
		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
			if (courseUpdated) {
				return courseUpdated; 
			} else {
				return 'Failed to Update Course';
			};
		}); 
	};

	module.exports.archiveCourse = (course) => {
	    let id = course.courseId; 
	    let updates = {
	   	  isActive: false 
	    } 
		return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return 'Course archived'; 
			} else {
				return false; 
			}
		});
	};

//[SECTION] Functionality [Delete]
	module.exports.deleteCourse = (courseId) => {
		return Course.findByIdAndRemove(courseId).then((removedCourse, err) => {
			if (removedCourse) {
				return 'Course successfully removed';
			} else {
				return 'No Course was Removed';
			};
		});
	};
