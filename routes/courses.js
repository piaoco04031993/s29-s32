//[SECTION] Dependencies and Modules
  const exp = require('express');
  const controller = require('../controllers/courses');
  const auth = require('../auth');

//[SECTION] Routing Component
  const route = exp.Router();
  
//[SECTION]-[POST] Route (Administrator Only)
  route.post('/', auth.verify ,(req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    let data = {
       isAdmin: isAdmin,
       course: req.body,
    }
    if (isAdmin) {
      controller.addCourse(data).then(outcome => {
        res.send(outcome);
      });
    } else {
      res.send('User Unauthorized to Proceed!');
    };    
  });

//[SECTION]-[GET] Route (Administrator Only)
  // run a middleware to verify the user  who will execute the task.
    route.get('/all', (req, res) => {
      // decode the token that will used to authorize the user in performing the task.
      let token = req.headers.authorization;
      // run thr token to the decode middleware to extract the payload.
      let payload = auth.decode(token);
      let isAdmin = payload.isAdmin
      // create a control structure that will give a response that depend upon the role of the user.
      isAdmin ? controller.getAllCourse().then(outcome => 
        res.send(outcome)) 
      : res.send9('Unathorized User');
    }); 

    route.get('/', (req, res) => {
      controller.getAllActive().then(outcome => {
        res.send(outcome);
      }); 
    });

    route.get('/:id', (req, res) => {
        let id = req.params.id
        controller.getCourse(id).then(result => {
          console.log('top',result)
          res.send(result); 

        });
    }); 

//[SECTION]-[PUT] Route Administrator
  //insert a middleware to verify the user
  route.put('/:courseId', auth.verify,(req, res) => {
    //this new approach that i will use can be used as an alternate approach to the preious method that was done.
    //identify if the user will have admin privileges
    let params = req.params;  
    let body = req.body; 
    controller.updateCourse(params, body).then(outcome => {
       res.send(outcome);
    });
    //catch the 'truthy' of the isAdmin
    if (auth.decode(req.headers.authorization).isAdmin) {
       //Admin 
       controller.updateCourse(params, body).then(outcome => {
           res.send(outcome);
       });
    } else {
       //'falsy' of the isAdmin
       //Non-Admin
       res.send('User Unauthorized');
    }
  });
  
  //Administrator Only
  route.put('/:courseId/archive', (req, res) => {
     let params = req.params; 
     controller.archiveCourse(params).then(result => {
        res.send(result);
     });
  });

//[SECTION]-[DEL] Route Administrator Only
  route.delete('/:courseId', auth.verify, (req, res) => {
     let isAdmin = auth.decode(req.headers.authorization).isAdmin;
     let id = req.params.courseId;
     isAdmin ? controller.deleteCourse(id).then(outcome => res.send(outcome))
     : res.send ('User Unauthorized!')
  }); 

//[SECTION] Export Route System 
  module.exports = route;
